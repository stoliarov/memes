package ru.nsu.stoliarov.memes;

public class Mem {
	private String title;
	private int likeCount;
	private int imageId;
	private Object attachment;
	
	public Mem(String title, int likeCount, int imageId) {
		this.title = title;
		this.likeCount = likeCount;
		this.imageId = imageId;
	}
	
	public void attach(Object object) {
		attachment = object;
	}
	
	public Object attachment() {
		return attachment;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getLikeCount() {
		return likeCount;
	}
	
	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}
	
	public int getImageId() {
		return imageId;
	}
	
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
}
