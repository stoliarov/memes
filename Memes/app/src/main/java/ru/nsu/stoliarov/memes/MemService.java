package ru.nsu.stoliarov.memes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemService {
	private static List<Mem> memes = new ArrayList<>();
	private static Map<Integer, Mem> likedMemes = new HashMap<>();
	
	public static List<Mem> getMemes() {
		return memes;
	}
	
	public static Map<Integer, Mem> getLikedMemes() {
		return likedMemes;
	}
	
	public static void fillMemes() {
		if(memes.isEmpty()) {
			memes.add(new Mem("Когда делаешь матан только с помощью китайского антидемидовича, " +
					"а потом Доманова ставит автомат 5",
					3, R.drawable.demidovich));
			memes.add(new Mem("Прямо из печи", 0, R.drawable.exam));
			memes.add(new Mem("Каждая лекция по матану выглядит примерно так",
					1, R.drawable.matan));
		}
	}
}
