package ru.nsu.stoliarov.memes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.nsu.stoliarov.memes.R;

public class ConfirmEmailActivity extends AppCompatActivity {
	public final static String LOG_TAG = "Confirm_email_Activity";
	
	private ConfirmEmailActivity currentActivity;
	
	private EditText code;
	private Button confirmCodeButton;
	
	private String saveCode;
	
	private String login_type = "";
	private String email = "";
	private String password = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_email);
		currentActivity = this;
		
		Intent intent = getIntent();
		login_type = intent.getStringExtra(Settings.LOGIN_TYPE_KEY);
		email = intent.getStringExtra(Settings.EMAIL_KEY);
		password = intent.getStringExtra(Settings.PASSWORD_KEY);
		
		configureView();
		setButtonsListeners();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		code.setText(saveCode);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		saveCode = String.valueOf(code.getText());
	}
	
	public void configureView() {
		code = findViewById(R.id.code_from_post);
		confirmCodeButton = findViewById(R.id.confirm_code_from_post_button);
	}
	
	public void setButtonsListeners() {
		confirmCodeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, MemesFeedActivity.class);
				intent.putExtra(Settings.LOGIN_TYPE_KEY, login_type);
				startActivity(intent);
			}
		});
	}
}
