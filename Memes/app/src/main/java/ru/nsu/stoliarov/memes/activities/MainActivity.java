package ru.nsu.stoliarov.memes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ru.nsu.stoliarov.memes.R;

public class MainActivity extends AppCompatActivity {
	public final static String LOG_TAG = "Main_Activity";
	
	private MainActivity currentActivity;
	
	//region UI Outlets
	private TextView signInAsAbiturDescription;
	private TextView signInAsStudentDescription;
	private Button signInAsGuest;
	private Button signInAsAbitur;
	private Button signInAsStudent;
	//endregion
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		currentActivity = this;
		
		configureView();
		setButtonsListeners();
		
		Log.d(LOG_TAG, "onCreate");
	}
	
	private void configureView() {
		signInAsAbiturDescription = findViewById(R.id.signInAsAbiturDescription);
		signInAsStudentDescription = findViewById(R.id.signInAsStudentDescription);
		signInAsGuest = findViewById(R.id.signInAsGuest);
		signInAsAbitur = findViewById(R.id.signInAsAbitur);
		signInAsStudent = findViewById(R.id.signInAsStudent);
	}
	
	private void setButtonsListeners() {
		signInAsGuest.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, MemesFeedActivity.class);
				intent.putExtra(Settings.LOGIN_TYPE_KEY, Settings.AS_GUEST);
				startActivity(intent);
			}
		});
		signInAsAbitur.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, RegistrationActivity.class);
				intent.putExtra(Settings.LOGIN_TYPE_KEY, Settings.AS_ABITUR);
				startActivity(intent);
			}
		});
		signInAsStudent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, RegistrationActivity.class);
				intent.putExtra(Settings.LOGIN_TYPE_KEY, Settings.AS_STUDENT);
				startActivity(intent);
			}
		});
	}
	
	// пользователь уже видит интерфейс (начиная с onStart) и теперь может жмакать кнопочки (получен фокус)
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	// пользователь свернул приложение
	// здесь фиксируем несохраненные данные, стопаем всякие видосики, запущенные в onResume
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
