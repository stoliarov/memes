package ru.nsu.stoliarov.memes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.nsu.stoliarov.memes.Mem;
import ru.nsu.stoliarov.memes.MemService;
import ru.nsu.stoliarov.memes.R;
import ru.nsu.stoliarov.memes.adapters.MemesFeedAdapter;

public class MemesFeedActivity extends AppCompatActivity {
	private MemesFeedAdapter memesFeedAdapter;
	private MemesFeedActivity currentActivity;
	
	private Button homeButton;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_memes_feed);
		currentActivity = this;
		
		MemService.fillMemes();
		memesFeedAdapter = new MemesFeedAdapter(this);
		
		ListView listView = findViewById(R.id.list_view_memes);
		listView.setAdapter(memesFeedAdapter);
		
		configureView();
		setButtonsListeners();
	}
	
	public void configureView() {
		homeButton = findViewById(R.id.button_home);
	}
	
	public void setButtonsListeners() {
		homeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, MemesFeedActivity.class);
				finish();
				startActivity(intent);
			}
		});
	}
}
