package ru.nsu.stoliarov.memes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.nsu.stoliarov.memes.R;

public class RegistrationActivity extends AppCompatActivity {
	public final static String LOG_TAG = "Registration_Activity";
	
	private String login_type = "";
	
	private RegistrationActivity currentActivity;
	
	//region UI Outlets
	private EditText email;
	private EditText password;
	private Button register;
	//endregion
	
	private String saveEmail;
	private String savePassword;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		currentActivity = this;
		
		Intent intent = getIntent();
		login_type = intent.getStringExtra(Settings.LOGIN_TYPE_KEY);
		
		configureView();
		setButtonsListeners();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		email.setText(saveEmail);
		password.setText(savePassword);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		saveEmail = String.valueOf(email.getText());
		savePassword = String.valueOf(password.getText());
	}
	
	public void configureView() {
		email = findViewById(R.id.input_email);
		password = findViewById(R.id.input_password);
		register = findViewById(R.id.register_button);
	}
	
	public void setButtonsListeners() {
		register.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(currentActivity, ConfirmEmailActivity.class);
				intent.putExtra(Settings.LOGIN_TYPE_KEY, login_type);
				intent.putExtra(Settings.EMAIL_KEY, email.getText());
				intent.putExtra(Settings.PASSWORD_KEY, password.getText());
				// todo  Где-нибудь здесь можно сгенерить код, отправить его на почту и передать
				// todo  в ConfirmEmailActivity, в котором проверить правильность ввода этого кода пользователем
				startActivity(intent);
			}
		});
	}
}
