package ru.nsu.stoliarov.memes.activities;

public class Settings {
	private Settings() {
	}
	
	//region Login type in extra messages
	// key
	public final static String LOGIN_TYPE_KEY = "login_type_key";
	// values
	public final static String AS_GUEST = "as_guest_value";
	public final static String AS_ABITUR = "as_abitur_value";
	public final static String AS_STUDENT = "as_student_value";
	//endregion
	
	//region Registration params of extra messages
	public final static String PASSWORD_KEY = "password_key";
	public final static String EMAIL_KEY = "email_key";
	//endregion
}
