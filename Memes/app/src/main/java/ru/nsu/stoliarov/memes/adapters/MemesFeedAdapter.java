package ru.nsu.stoliarov.memes.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.nsu.stoliarov.memes.Mem;
import ru.nsu.stoliarov.memes.MemService;
import ru.nsu.stoliarov.memes.R;

public class MemesFeedAdapter extends BaseAdapter {
	public final static String LOG_TAG = "MemesFeedAdapter";
	
	private Context context;
	private LayoutInflater layoutInflater;
	private ArrayList<Mem> memes;
	private Map<Integer, Mem> likedMemes;
	
	public MemesFeedAdapter(Context context) {
		this.context = context;
		this.memes = (ArrayList<Mem>) MemService.getMemes();
		this.likedMemes = MemService.getLikedMemes();
		this.layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return memes.size();
	}
	
	@Override
	public Object getItem(int position) {
		return memes.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	private static class ViewHolder {
		TextView title;
		TextView likeCount;
		ImageButton likeButton;
		ImageView image;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		
		if(null == convertView) {
			convertView = layoutInflater.inflate(R.layout.item, parent, false);
			
			viewHolder = new ViewHolder();
			viewHolder.likeCount = convertView.findViewById(R.id.like_count);
			viewHolder.likeButton = convertView.findViewById(R.id.like_button);
			viewHolder.title = convertView.findViewById(R.id.mem_title);
			viewHolder.image = convertView.findViewById(R.id.mem_image);
			
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		final Mem mem = memes.get(position);
		viewHolder.likeCount.setText(String.valueOf(mem.getLikeCount()));
		mem.attach(viewHolder);
		
		viewHolder.likeButton.setContentDescription(String.valueOf(position));
		if(likedMemes.containsKey(position)) {
			viewHolder.likeCount.setTextColor(Color.rgb(218, 0, 0));
			viewHolder.likeButton.setImageResource(R.drawable.red_like);
		}
		viewHolder.likeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ViewHolder viewHolder = (ViewHolder) mem.attachment();
				
				int position = Integer.valueOf(String.valueOf(viewHolder.likeButton.getContentDescription()));
				
				Mem mem = memes.get(position);
				
				if(likedMemes.containsKey(position)) {
					likedMemes.remove(position);
					mem.setLikeCount(mem.getLikeCount() - 1);
					viewHolder.likeCount.setText(String.valueOf(mem.getLikeCount()));
					viewHolder.likeCount.setTextColor(Color.BLACK);
					viewHolder.likeButton.setImageResource(R.drawable.like);
				} else {
					likedMemes.put(position, mem);
					mem.setLikeCount(mem.getLikeCount() + 1);
					viewHolder.likeCount.setText(String.valueOf(mem.getLikeCount()));
					viewHolder.likeCount.setTextColor(Color.rgb(218, 0, 0));
					viewHolder.likeButton.setImageResource(R.drawable.red_like);
				}
			}
		});
		
		viewHolder.title.setText(mem.getTitle());
		viewHolder.image.setImageResource(mem.getImageId());
		
		return convertView;
	}
}
